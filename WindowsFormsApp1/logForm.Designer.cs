﻿namespace WindowsFormsApp1
{
    partial class logForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnSaveLog = new System.Windows.Forms.Button();
            this.showLog = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.showLog)).BeginInit();
            this.SuspendLayout();
            // 
            // btnSaveLog
            // 
            this.btnSaveLog.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSaveLog.Location = new System.Drawing.Point(648, 12);
            this.btnSaveLog.Name = "btnSaveLog";
            this.btnSaveLog.Size = new System.Drawing.Size(108, 34);
            this.btnSaveLog.TabIndex = 2;
            this.btnSaveLog.Text = "ذخیره";
            this.btnSaveLog.UseVisualStyleBackColor = true;
            this.btnSaveLog.Click += new System.EventHandler(this.btnSaveLog_Click);
            // 
            // showLog
            // 
            this.showLog.AllowUserToAddRows = false;
            this.showLog.AllowUserToDeleteRows = false;
            this.showLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.showLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.showLog.Location = new System.Drawing.Point(12, 53);
            this.showLog.Name = "showLog";
            this.showLog.ReadOnly = true;
            this.showLog.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.showLog.Size = new System.Drawing.Size(744, 256);
            this.showLog.TabIndex = 3;
            // 
            // logForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(768, 321);
            this.Controls.Add(this.showLog);
            this.Controls.Add(this.btnSaveLog);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "logForm";
            this.Text = "سوابق تردد";
            this.Load += new System.EventHandler(this.logForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.showLog)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Button btnSaveLog;
        private System.Windows.Forms.DataGridView showLog;
    }
}