﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApp1.DataLayer
{
    public class Commute
    {
        public Commute()
        {

        }
        public Commute(string FirstName, string LastName, string StuNumber, string Major, string Date, bool State)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.StuNumber = StuNumber;
            this.Major = Major;
            this.Date = Date;
            this.State = State;
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string StuNumber { get; set; }
        public string Major { get; set; }
        public string Date { get; set; }
        public bool State { get; set; }
    }
}
