﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
namespace WindowsFormsApp1.DataLayer
{
    public class DataBase
    {
        static SQLiteConnection client = null;
        public DataBase()
        {
            if (client == null)
            {
                client = CreateConnection();
            }
            CreateTable(client);
        }
        SQLiteConnection CreateConnection()
        {
            SQLiteConnection sqlite_conn;
            // Create a new database connection:
            sqlite_conn = new SQLiteConnection("Data Source=database.db; Version = 3; New = True; Compress = True; ");
            // Open the connection:
            try
            {
                sqlite_conn.Open();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            return sqlite_conn;
        }
        void CreateTable(SQLiteConnection conn)
        {
            SQLiteCommand sqlite_cmd;
            string Createsql = "CREATE TABLE IF NOT EXISTS student(id INTEGER PRIMARY KEY, fName varchar(255), lName varchar(255), stuNumber varchar(255), stuMajor varchar(255), stuPhoto varchar(255), state BOOLEAN)";
            string Createsql1 = "CREATE TABLE IF NOT EXISTS commute(id INTEGER PRIMARY KEY, fName varchar(255), lName varchar(255), stuNumber varchar(255), stuMajor varchar(255), state BOOLEAN, date varchar(255))";
            sqlite_cmd = conn.CreateCommand();
            sqlite_cmd.CommandText = Createsql;
            sqlite_cmd.ExecuteNonQuery();
            sqlite_cmd.CommandText = Createsql1;
            sqlite_cmd.ExecuteNonQuery();
        }
        public void InsertStudent(Student student)
        {
            var sqlite_cmd = client.CreateCommand();
            sqlite_cmd.CommandText = $"INSERT INTO student (fName, lName, stuNumber, stuMajor, state, stuPhoto) VALUES('{student.FirstName}', '{student.LastName}', '{student.StuNumber}', '{student.Major}', FALSE, '{student.PhotoPath}'); ";
            sqlite_cmd.ExecuteNonQuery();
        }
        public Student GetStudent(string stuNumber)
        {
            string stm = $"SELECT * FROM student WHERE stuNumber = '{stuNumber}'";

            using (SQLiteCommand cmd = new SQLiteCommand(stm, client))
            {
                using (SQLiteDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Student student = new Student();
                        student.FirstName = rdr.IsDBNull(1) ? null : rdr.GetString(1);
                        student.LastName = rdr.IsDBNull(2) ? null : rdr.GetString(2);
                        student.StuNumber = rdr.IsDBNull(3) ? null : rdr.GetString(3);
                        student.Major = rdr.IsDBNull(4) ? null : rdr.GetString(4);
                        student.PhotoPath = rdr.IsDBNull(5) ? null : rdr.GetString(5);
                        student.State = rdr.GetBoolean(6);
                        return student;
                    }
                }
            }

            return null;
        }
        public void DeleteStudent(string stuNumber)
        {
            var sqlite_cmd = client.CreateCommand();
            sqlite_cmd.CommandText = $"DELETE FROM student WHERE stuNumber = '{stuNumber}'";
            sqlite_cmd.ExecuteNonQuery();
        }
        public void EditStudent(Student student, string stuNumber)
        {
            var sqlite_cmd = client.CreateCommand();
            sqlite_cmd.CommandText = $"UPDATE student SET fName = '{student.FirstName}', lName = '{student.LastName}', stuNumber = '{student.StuNumber}', stuMajor = '{student.Major}', stuPhoto = '{student.PhotoPath}'  WHERE stuNumber = '{stuNumber}'";
            sqlite_cmd.ExecuteNonQuery();
        }
        public void SetStudentStatus(string stuNumber, bool state)
        {
            var sqlite_cmd = client.CreateCommand();
            sqlite_cmd.CommandText = $"UPDATE student SET state = {state} WHERE stuNumber = '{stuNumber}'";
            sqlite_cmd.ExecuteNonQuery();
        }
        public void InsertCommute(Commute commute)
        {
            var sqlite_cmd = client.CreateCommand();
            sqlite_cmd.CommandText = $"INSERT INTO commute (fName, lName, stuNumber, stuMajor, state, date) VALUES('{commute.FirstName}', '{commute.LastName}', '{commute.StuNumber}', '{commute.Major}', {commute.State}, '{commute.Date}'); ";
            sqlite_cmd.ExecuteNonQuery();
        }
        public List<Commute> GetAllCommutes()
        {
            string stm = $"SELECT * FROM commute";
            List<Commute> commuteList = new List<Commute>();
            using (SQLiteCommand cmd = new SQLiteCommand(stm, client))
            {
                using (SQLiteDataReader rdr = cmd.ExecuteReader())
                {
                    while (rdr.Read())
                    {
                        Commute commute = new Commute();
                        commute.FirstName = rdr.IsDBNull(1) ? null : rdr.GetString(1);
                        commute.LastName = rdr.IsDBNull(2) ? null : rdr.GetString(2);
                        commute.StuNumber = rdr.IsDBNull(3) ? null : rdr.GetString(3);
                        commute.Major = rdr.IsDBNull(4) ? null : rdr.GetString(4);
                        commute.State = rdr.GetBoolean(5);
                        commute.Date = rdr.IsDBNull(6) ? null : rdr.GetString(6);
                        commuteList.Add(commute);
                    }
                }
            }
            return commuteList;
        }
    }
}
