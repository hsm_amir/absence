﻿namespace WindowsFormsApp1
{
    partial class mainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainForm));
            this.btnStuNumber = new System.Windows.Forms.Button();
            this.stuNumberInput = new System.Windows.Forms.TextBox();
            this.lblInputNumber = new System.Windows.Forms.Label();
            this.lblTimeNow = new System.Windows.Forms.Label();
            this.lblDateNow = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBoxStuCard = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblStuLname = new System.Windows.Forms.Label();
            this.lblStuNumber = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblStuMajor = new System.Windows.Forms.Label();
            this.lblStuFname = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblStuStatus = new System.Windows.Forms.Label();
            this.imgStu = new System.Windows.Forms.PictureBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.btnManStu = new System.Windows.Forms.Button();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.groupBoxStuCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStu)).BeginInit();
            this.SuspendLayout();
            // 
            // btnStuNumber
            // 
            this.btnStuNumber.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            resources.ApplyResources(this.btnStuNumber, "btnStuNumber");
            this.btnStuNumber.Name = "btnStuNumber";
            this.btnStuNumber.UseVisualStyleBackColor = false;
            this.btnStuNumber.Click += new System.EventHandler(this.btnStuNumber_Click);
            // 
            // stuNumberInput
            // 
            resources.ApplyResources(this.stuNumberInput, "stuNumberInput");
            this.stuNumberInput.Name = "stuNumberInput";
            this.stuNumberInput.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stuNumberInput_KeyPress);
            // 
            // lblInputNumber
            // 
            resources.ApplyResources(this.lblInputNumber, "lblInputNumber");
            this.lblInputNumber.Name = "lblInputNumber";
            // 
            // lblTimeNow
            // 
            resources.ApplyResources(this.lblTimeNow, "lblTimeNow");
            this.lblTimeNow.Name = "lblTimeNow";
            // 
            // lblDateNow
            // 
            resources.ApplyResources(this.lblDateNow, "lblDateNow");
            this.lblDateNow.Name = "lblDateNow";
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick_1);
            // 
            // groupBoxStuCard
            // 
            this.groupBoxStuCard.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBoxStuCard.Controls.Add(this.label1);
            this.groupBoxStuCard.Controls.Add(this.lblStuLname);
            this.groupBoxStuCard.Controls.Add(this.lblStuNumber);
            this.groupBoxStuCard.Controls.Add(this.label2);
            this.groupBoxStuCard.Controls.Add(this.lblStuMajor);
            this.groupBoxStuCard.Controls.Add(this.lblStuFname);
            this.groupBoxStuCard.Controls.Add(this.label3);
            this.groupBoxStuCard.Controls.Add(this.lblStuStatus);
            this.groupBoxStuCard.Controls.Add(this.imgStu);
            this.groupBoxStuCard.Controls.Add(this.label4);
            resources.ApplyResources(this.groupBoxStuCard, "groupBoxStuCard");
            this.groupBoxStuCard.Name = "groupBoxStuCard";
            this.groupBoxStuCard.TabStop = false;
            // 
            // label1
            // 
            resources.ApplyResources(this.label1, "label1");
            this.label1.Name = "label1";
            // 
            // lblStuLname
            // 
            resources.ApplyResources(this.lblStuLname, "lblStuLname");
            this.lblStuLname.Name = "lblStuLname";
            // 
            // lblStuNumber
            // 
            resources.ApplyResources(this.lblStuNumber, "lblStuNumber");
            this.lblStuNumber.Name = "lblStuNumber";
            // 
            // label2
            // 
            resources.ApplyResources(this.label2, "label2");
            this.label2.Name = "label2";
            // 
            // lblStuMajor
            // 
            resources.ApplyResources(this.lblStuMajor, "lblStuMajor");
            this.lblStuMajor.Name = "lblStuMajor";
            // 
            // lblStuFname
            // 
            resources.ApplyResources(this.lblStuFname, "lblStuFname");
            this.lblStuFname.Name = "lblStuFname";
            // 
            // label3
            // 
            resources.ApplyResources(this.label3, "label3");
            this.label3.Name = "label3";
            // 
            // lblStuStatus
            // 
            resources.ApplyResources(this.lblStuStatus, "lblStuStatus");
            this.lblStuStatus.Name = "lblStuStatus";
            // 
            // imgStu
            // 
            this.imgStu.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            resources.ApplyResources(this.imgStu, "imgStu");
            this.imgStu.Image = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            this.imgStu.Name = "imgStu";
            this.imgStu.TabStop = false;
            // 
            // label4
            // 
            resources.ApplyResources(this.label4, "label4");
            this.label4.Name = "label4";
            // 
            // btnLogin
            // 
            resources.ApplyResources(this.btnLogin, "btnLogin");
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // btnManStu
            // 
            resources.ApplyResources(this.btnManStu, "btnManStu");
            this.btnManStu.Name = "btnManStu";
            this.btnManStu.UseVisualStyleBackColor = true;
            this.btnManStu.Click += new System.EventHandler(this.btnManStu_Click);
            // 
            // timer2
            // 
            this.timer2.Interval = 5000;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // mainForm
            // 
            resources.ApplyResources(this, "$this");
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.btnManStu);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.groupBoxStuCard);
            this.Controls.Add(this.lblDateNow);
            this.Controls.Add(this.lblTimeNow);
            this.Controls.Add(this.lblInputNumber);
            this.Controls.Add(this.stuNumberInput);
            this.Controls.Add(this.btnStuNumber);
            this.Cursor = System.Windows.Forms.Cursors.Default;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "mainForm";
            this.groupBoxStuCard.ResumeLayout(false);
            this.groupBoxStuCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStu)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnStuNumber;
        private System.Windows.Forms.TextBox stuNumberInput;
        private System.Windows.Forms.Label lblInputNumber;
        private System.Windows.Forms.Label lblTimeNow;
        private System.Windows.Forms.Label lblDateNow;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.GroupBox groupBoxStuCard;
        private System.Windows.Forms.PictureBox imgStu;
        private System.Windows.Forms.Label lblStuStatus;
        private System.Windows.Forms.Label lblStuLname;
        private System.Windows.Forms.Label lblStuNumber;
        private System.Windows.Forms.Label lblStuMajor;
        private System.Windows.Forms.Label lblStuFname;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.Button btnManStu;
        private System.Windows.Forms.Timer timer2;
    }
}

