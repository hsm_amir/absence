﻿namespace WindowsFormsApp1
{
    partial class manageStuForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.inputStuFname = new System.Windows.Forms.TextBox();
            this.inputStuLname = new System.Windows.Forms.TextBox();
            this.inputStuMajor = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.SelectedImg = new System.Windows.Forms.PictureBox();
            this.btnAddNewStu = new System.Windows.Forms.Button();
            this.inputStuNumber = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnStuNumberSearch = new System.Windows.Forms.Button();
            this.stuNumberSearch = new System.Windows.Forms.TextBox();
            this.lblInputNumberSearch = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBoxStuCard = new System.Windows.Forms.GroupBox();
            this.editStuMajor = new System.Windows.Forms.TextBox();
            this.editStuNumber = new System.Windows.Forms.TextBox();
            this.editStuLname = new System.Windows.Forms.TextBox();
            this.editStuFname = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.imgStuEdit = new System.Windows.Forms.PictureBox();
            this.label11 = new System.Windows.Forms.Label();
            this.btnStuEdit = new System.Windows.Forms.Button();
            this.btnStuDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SelectedImg)).BeginInit();
            this.groupBoxStuCard.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStuEdit)).BeginInit();
            this.SuspendLayout();
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(609, 53);
            this.label4.Name = "label4";
            this.label4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label4.Size = new System.Drawing.Size(70, 21);
            this.label4.TabIndex = 11;
            this.label4.Text = "نام دانشجو:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(594, 131);
            this.label3.Name = "label3";
            this.label3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label3.Size = new System.Drawing.Size(85, 21);
            this.label3.TabIndex = 12;
            this.label3.Text = "رشته تحصیلی:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(579, 105);
            this.label2.Name = "label2";
            this.label2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label2.Size = new System.Drawing.Size(100, 21);
            this.label2.TabIndex = 13;
            this.label2.Text = "شماره دانشجویی:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(557, 79);
            this.label1.Name = "label1";
            this.label1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label1.Size = new System.Drawing.Size(122, 21);
            this.label1.TabIndex = 14;
            this.label1.Text = "نام خانوادگی دانشجو:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // inputStuFname
            // 
            this.inputStuFname.Location = new System.Drawing.Point(435, 53);
            this.inputStuFname.Name = "inputStuFname";
            this.inputStuFname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.inputStuFname.Size = new System.Drawing.Size(116, 20);
            this.inputStuFname.TabIndex = 1;
            // 
            // inputStuLname
            // 
            this.inputStuLname.Location = new System.Drawing.Point(435, 79);
            this.inputStuLname.Name = "inputStuLname";
            this.inputStuLname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.inputStuLname.Size = new System.Drawing.Size(116, 20);
            this.inputStuLname.TabIndex = 2;
            // 
            // inputStuMajor
            // 
            this.inputStuMajor.Location = new System.Drawing.Point(435, 131);
            this.inputStuMajor.Name = "inputStuMajor";
            this.inputStuMajor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.inputStuMajor.Size = new System.Drawing.Size(116, 20);
            this.inputStuMajor.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(592, 157);
            this.label5.Name = "label5";
            this.label5.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label5.Size = new System.Drawing.Size(87, 21);
            this.label5.TabIndex = 19;
            this.label5.Text = "تصویر دانشجو:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // SelectedImg
            // 
            this.SelectedImg.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            this.SelectedImg.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.SelectedImg.Image = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            this.SelectedImg.Location = new System.Drawing.Point(435, 157);
            this.SelectedImg.Name = "SelectedImg";
            this.SelectedImg.Size = new System.Drawing.Size(116, 127);
            this.SelectedImg.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.SelectedImg.TabIndex = 21;
            this.SelectedImg.TabStop = false;
            this.SelectedImg.Click += new System.EventHandler(this.SelectedImg_Click);
            // 
            // btnAddNewStu
            // 
            this.btnAddNewStu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnAddNewStu.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddNewStu.Location = new System.Drawing.Point(435, 290);
            this.btnAddNewStu.Name = "btnAddNewStu";
            this.btnAddNewStu.Size = new System.Drawing.Size(240, 25);
            this.btnAddNewStu.TabIndex = 5;
            this.btnAddNewStu.Text = "ثبت دانشجو";
            this.btnAddNewStu.UseVisualStyleBackColor = false;
            this.btnAddNewStu.Click += new System.EventHandler(this.btnAddNewStu_Click);
            // 
            // inputStuNumber
            // 
            this.inputStuNumber.Location = new System.Drawing.Point(435, 105);
            this.inputStuNumber.MaxLength = 10;
            this.inputStuNumber.Name = "inputStuNumber";
            this.inputStuNumber.Size = new System.Drawing.Size(116, 20);
            this.inputStuNumber.TabIndex = 3;
            this.inputStuNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.inputStuNumber_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("IRANSans(FaNum)", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(475, 9);
            this.label6.Name = "label6";
            this.label6.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label6.Size = new System.Drawing.Size(148, 25);
            this.label6.TabIndex = 23;
            this.label6.Text = "ثبت دانشجو جدید";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnStuNumberSearch
            // 
            this.btnStuNumberSearch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnStuNumberSearch.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStuNumberSearch.Location = new System.Drawing.Point(20, 51);
            this.btnStuNumberSearch.Name = "btnStuNumberSearch";
            this.btnStuNumberSearch.Size = new System.Drawing.Size(75, 25);
            this.btnStuNumberSearch.TabIndex = 7;
            this.btnStuNumberSearch.Text = "جستجو";
            this.btnStuNumberSearch.UseVisualStyleBackColor = false;
            this.btnStuNumberSearch.Click += new System.EventHandler(this.btnStuNumberSearch_Click);
            // 
            // stuNumberSearch
            // 
            this.stuNumberSearch.Location = new System.Drawing.Point(101, 54);
            this.stuNumberSearch.MaxLength = 10;
            this.stuNumberSearch.Name = "stuNumberSearch";
            this.stuNumberSearch.Size = new System.Drawing.Size(159, 20);
            this.stuNumberSearch.TabIndex = 6;
            this.stuNumberSearch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.stuNumberSearch_KeyPress);
            // 
            // lblInputNumberSearch
            // 
            this.lblInputNumberSearch.AutoSize = true;
            this.lblInputNumberSearch.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInputNumberSearch.Location = new System.Drawing.Point(260, 54);
            this.lblInputNumberSearch.Name = "lblInputNumberSearch";
            this.lblInputNumberSearch.Size = new System.Drawing.Size(155, 19);
            this.lblInputNumberSearch.TabIndex = 5;
            this.lblInputNumberSearch.Text = ":شماره دانشجویی را وارد  کنید";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("IRANSans(FaNum)", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(121, 9);
            this.label7.Name = "label7";
            this.label7.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label7.Size = new System.Drawing.Size(187, 25);
            this.label7.TabIndex = 24;
            this.label7.Text = "ویرایش و حذف دانشجو";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // groupBoxStuCard
            // 
            this.groupBoxStuCard.BackColor = System.Drawing.SystemColors.ControlLight;
            this.groupBoxStuCard.Controls.Add(this.editStuMajor);
            this.groupBoxStuCard.Controls.Add(this.editStuNumber);
            this.groupBoxStuCard.Controls.Add(this.editStuLname);
            this.groupBoxStuCard.Controls.Add(this.editStuFname);
            this.groupBoxStuCard.Controls.Add(this.label8);
            this.groupBoxStuCard.Controls.Add(this.label9);
            this.groupBoxStuCard.Controls.Add(this.label10);
            this.groupBoxStuCard.Controls.Add(this.imgStuEdit);
            this.groupBoxStuCard.Controls.Add(this.label11);
            this.groupBoxStuCard.Location = new System.Drawing.Point(20, 82);
            this.groupBoxStuCard.Name = "groupBoxStuCard";
            this.groupBoxStuCard.Size = new System.Drawing.Size(395, 126);
            this.groupBoxStuCard.TabIndex = 25;
            this.groupBoxStuCard.TabStop = false;
            // 
            // editStuMajor
            // 
            this.editStuMajor.Location = new System.Drawing.Point(107, 90);
            this.editStuMajor.Name = "editStuMajor";
            this.editStuMajor.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.editStuMajor.Size = new System.Drawing.Size(176, 20);
            this.editStuMajor.TabIndex = 11;
            // 
            // editStuNumber
            // 
            this.editStuNumber.Location = new System.Drawing.Point(107, 64);
            this.editStuNumber.MaxLength = 10;
            this.editStuNumber.Name = "editStuNumber";
            this.editStuNumber.Size = new System.Drawing.Size(161, 20);
            this.editStuNumber.TabIndex = 10;
            this.editStuNumber.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox3_KeyPress);
            // 
            // editStuLname
            // 
            this.editStuLname.Location = new System.Drawing.Point(107, 38);
            this.editStuLname.Name = "editStuLname";
            this.editStuLname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.editStuLname.Size = new System.Drawing.Size(134, 20);
            this.editStuLname.TabIndex = 9;
            // 
            // editStuFname
            // 
            this.editStuFname.Location = new System.Drawing.Point(107, 12);
            this.editStuFname.Name = "editStuFname";
            this.editStuFname.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.editStuFname.Size = new System.Drawing.Size(195, 20);
            this.editStuFname.TabIndex = 8;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(247, 38);
            this.label8.Name = "label8";
            this.label8.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label8.Size = new System.Drawing.Size(142, 21);
            this.label8.TabIndex = 10;
            this.label8.Text = "نام خانوادگی دانشجو:";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(274, 64);
            this.label9.Name = "label9";
            this.label9.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label9.Size = new System.Drawing.Size(115, 21);
            this.label9.TabIndex = 9;
            this.label9.Text = "شماره دانشجویی:";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(292, 90);
            this.label10.Name = "label10";
            this.label10.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label10.Size = new System.Drawing.Size(97, 21);
            this.label10.TabIndex = 8;
            this.label10.Text = "رشته تحصیلی:";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // imgStuEdit
            // 
            this.imgStuEdit.BackgroundImage = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            this.imgStuEdit.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.imgStuEdit.Image = global::WindowsFormsApp1.Properties.Resources.stuDefualt;
            this.imgStuEdit.Location = new System.Drawing.Point(7, 11);
            this.imgStuEdit.Name = "imgStuEdit";
            this.imgStuEdit.Size = new System.Drawing.Size(94, 100);
            this.imgStuEdit.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.imgStuEdit.TabIndex = 0;
            this.imgStuEdit.TabStop = false;
            this.imgStuEdit.Click += new System.EventHandler(this.imgStuEdit_Click);
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("IRANSans(FaNum)", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(308, 12);
            this.label11.Name = "label11";
            this.label11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label11.Size = new System.Drawing.Size(81, 21);
            this.label11.TabIndex = 7;
            this.label11.Text = "نام دانشجو:";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // btnStuEdit
            // 
            this.btnStuEdit.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnStuEdit.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStuEdit.Location = new System.Drawing.Point(20, 214);
            this.btnStuEdit.Name = "btnStuEdit";
            this.btnStuEdit.Size = new System.Drawing.Size(395, 23);
            this.btnStuEdit.TabIndex = 12;
            this.btnStuEdit.Text = "ویرایش دانشجو";
            this.btnStuEdit.UseVisualStyleBackColor = false;
            this.btnStuEdit.Click += new System.EventHandler(this.btnStuEdit_Click);
            // 
            // btnStuDelete
            // 
            this.btnStuDelete.BackColor = System.Drawing.Color.Red;
            this.btnStuDelete.Font = new System.Drawing.Font("IRANSans(FaNum)", 8.249999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStuDelete.Location = new System.Drawing.Point(20, 243);
            this.btnStuDelete.Name = "btnStuDelete";
            this.btnStuDelete.Size = new System.Drawing.Size(395, 23);
            this.btnStuDelete.TabIndex = 13;
            this.btnStuDelete.Text = "حذف دانشجو";
            this.btnStuDelete.UseVisualStyleBackColor = false;
            this.btnStuDelete.Click += new System.EventHandler(this.btnStuDelete_Click);
            // 
            // manageStuForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 322);
            this.Controls.Add(this.btnStuDelete);
            this.Controls.Add(this.btnStuEdit);
            this.Controls.Add(this.groupBoxStuCard);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.lblInputNumberSearch);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.stuNumberSearch);
            this.Controls.Add(this.inputStuNumber);
            this.Controls.Add(this.btnStuNumberSearch);
            this.Controls.Add(this.btnAddNewStu);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.SelectedImg);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.inputStuMajor);
            this.Controls.Add(this.inputStuFname);
            this.Controls.Add(this.inputStuLname);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Name = "manageStuForm";
            this.Text = "مدیریت دانشجویان";
            ((System.ComponentModel.ISupportInitialize)(this.SelectedImg)).EndInit();
            this.groupBoxStuCard.ResumeLayout(false);
            this.groupBoxStuCard.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.imgStuEdit)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox inputStuFname;
        private System.Windows.Forms.TextBox inputStuLname;
        private System.Windows.Forms.TextBox inputStuMajor;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.PictureBox SelectedImg;
        private System.Windows.Forms.Button btnAddNewStu;
        private System.Windows.Forms.TextBox inputStuNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button btnStuNumberSearch;
        private System.Windows.Forms.TextBox stuNumberSearch;
        private System.Windows.Forms.Label lblInputNumberSearch;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBoxStuCard;
        private System.Windows.Forms.TextBox editStuMajor;
        private System.Windows.Forms.TextBox editStuNumber;
        private System.Windows.Forms.TextBox editStuLname;
        private System.Windows.Forms.TextBox editStuFname;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.PictureBox imgStuEdit;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnStuEdit;
        private System.Windows.Forms.Button btnStuDelete;
    }
}