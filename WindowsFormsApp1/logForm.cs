﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DataLayer;
using OfficeOpenXml;
using System.IO;

namespace WindowsFormsApp1
{
    public partial class logForm : Form
    {
        DataBase db;
        public logForm()
        {
            InitializeComponent();
            db = new DataBase();
        }

        private void logForm_Load(object sender, EventArgs e)
        {
            var commutes = db.GetAllCommutes();
            showLog.ColumnCount = 7;
            showLog.Columns[0].Name = "ردیف";
            showLog.Columns[1].Name = "نام";
            showLog.Columns[2].Name = "نام خانوادگی";
            showLog.Columns[3].Name = "شماره دانشجویی";
            showLog.Columns[4].Name = "رشته";
            showLog.Columns[5].Name = "وضعیت";
            showLog.Columns[6].Name = "زمان";

            int count = 1;
            foreach (var commute in commutes)
            {
                showLog.Rows.Add(count.ToString(), commute.FirstName, commute.LastName, commute.StuNumber, commute.Major, commute.State ? "ورود" : "خروج", commute.Date);
                count++;
            }

        }


        private void btnSaveLog_Click(object sender, EventArgs e)
        {
            SaveFileDialog excelFile = new SaveFileDialog();
            excelFile.FileName = DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";
            excelFile.Filter = "xlsx files (*.xlsx)|*.xlsx|All files (*.*)|*.*";

            if (excelFile.ShowDialog() == DialogResult.OK)
            {
                var commutes = db.GetAllCommutes();
                using (ExcelPackage excel = new ExcelPackage())
                {
                    var workSheet = excel.Workbook.Worksheets.Add("Commute");
                    var headerRow = new List<string[]>() { new string[] { "ردیف", "نام", "نام خانوادگی", "شماره دانشجویی", "رشته", "وضعیت", "زمان" } };
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";
                    workSheet.Cells[headerRange].LoadFromArrays(headerRow);

                    var data = new List<object[]>();
                    int count = 1;
                    foreach (var commute in commutes)
                    {
                        data.Add(new object[] { count.ToString(), commute.FirstName, commute.LastName, commute.StuNumber, commute.Major, commute.State ? "ورود" : "خروج", commute.Date });
                        count++;
                    }
                    workSheet.Cells[2, 1].LoadFromArrays(data);

                    FileInfo fileInfo = new FileInfo(excelFile.FileName);
                    excel.SaveAs(fileInfo);
                }
            }
        }

    }
}
