﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using WindowsFormsApp1.DataLayer;

namespace WindowsFormsApp1
{
    public partial class mainForm : Form
    {
        DataBase db;
        public mainForm()
        {
            InitializeComponent();
            db = new DataBase();
            //db.InsertStudent(new Student("sobi", "amir", "0987654312", "mdkjsavf", "", false));
        }
        private void timer1_Tick_1(object sender, EventArgs e)
        {
            lblTimeNow.Text = DateTime.Now.ToString("HH:mm:ss");
            lblDateNow.Text = DateTime.Now.ToLongDateString();
        }
        private void stuNumberInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsNumber(e.KeyChar))
                e.Handled = e.KeyChar != (char)Keys.Back;
        }
        private void btnLogin_Click(object sender, EventArgs e)
        {
            new logForm().Show();
        }
        private void btnManStu_Click(object sender, EventArgs e)
        {
            new manageStuForm().Show();
        }
        private void btnStuNumber_Click(object sender, EventArgs e)
        {
            string stuNumber = stuNumberInput.Text;
            if (stuNumber == "")
            {
                MessageBox.Show("شماره دانشجویی را وارد کنید", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            var student = db.GetStudent(stuNumber);
            if (student == null)
            {
                MessageBox.Show("شماره دانشجویی صحیح نیست", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            stuNumberInput.Text = "";
            lblStuFname.Text = student.FirstName;
            lblStuLname.Text = student.LastName;
            lblStuNumber.Text = student.StuNumber;
            lblStuMajor.Text = student.Major;
            imgStu.ImageLocation = Path.Combine(Environment.CurrentDirectory, student.PhotoPath);
            if (student.State)
            {
                lblStuStatus.ForeColor = Color.Red;
                lblStuStatus.Text = ("دانشجو خارج شد");
            }
            else
            {
                lblStuStatus.ForeColor = Color.Green;
                lblStuStatus.Text = ("دانشجو وارد شد");
            }

            db.SetStudentStatus(stuNumber, !student.State);
            db.InsertCommute(new Commute(student.FirstName, student.LastName, student.StuNumber, student.Major, DateTime.Now.ToString("MM/dd/yyyy HH:mm"), !student.State));
            timer2.Start();
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            timer2.Stop();
            lblStuFname.Text = "";
            lblStuLname.Text = "";
            lblStuNumber.Text = "";
            lblStuMajor.Text = "";
            imgStu.ImageLocation = "";
            lblStuStatus.Text = "";
        }
    }
}
